#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from statistics import mode
datos = []


# Transforma el archivo a una lista
def listar_archivo(datos):

    with open("top50.csv") as file_:
        for line in file_:
            datos.append(line.split(","))
    # print(datos)


# se aisla i en la posicion 2
# corresponde a una lista de solo artista
def artistas(datos):

    artist_list = []

    for i in datos:
        artist_list.append(i[2])
    # eliminamos el indice 0 ya que no es una valor
    del artist_list[0]
    # print(artist_list)

    # funcion para eliminar cantantes que se repitan
    # y sacar un total
    eliminar_repetidos(artist_list)
    repetido = artist_list
    print("el artista  mas repetido es:", mode(repetido))

# elimina artistas repetidos
# crea nueva lista sin repetir artistas
def eliminar_repetidos(artist_list):

    new_artist_list = []

    for i in artist_list:
        if i not in new_artist_list:
            new_artist_list.append(i)
    #print(new_artist_list)
    # sacamos largo y sumamos
    suma = len(new_artist_list)
    print("la cantidad de artistas es:", suma)


def ruido(datos):

    list_ruido = []
    # creamos la lista de la columna correspondiente a ruidos
    for i in datos:
        list_ruido.append(i[7])
    # eliminamos el indice de letras
    del list_ruido[0]
    # print(list_ruido)
    list_ruido.sort()
    # print(list_ruido)
    mediana = len(list_ruido) / 2
    print("la mediana esta en la posicion:", mediana)
    print("con valor:", list_ruido[25])
    #print(" la mediana es:", mediana)


# funcion que determinaría los 3 mas bailables
def mas_bailables(datos):

    bailables = []

    for i in datos:
        bailables.append(i[6])
    del bailables[0]
    # print(bailables)
    int_bailables = bailables
    # print(int_bailables)
    analizar_bailable(bailables)


# se analiza la lista creada para definir mas bailables
def analizar_bailable(int_bailables):

    nuevo_bailable = []

    for i in int_bailables:
        if i not in nuevo_bailable:
            nuevo_bailable.append(i)
    # se saca el max para ver tres canciones mas populares
    print(max(nuevo_bailable))


# funcion determina si hay o no cancion mas popolar y menos ruidosa
def maspopu_menosruidosa(datos):

    list_popu = []
    ruido2 = []
    contador = 0
    # se generan lista de la columna 13 y 7 respectivamente
    for i in datos:
        list_popu.append(i[13])
    del list_popu[0]
    # se guarda el valor maximo para comparar
    maximo = max(list_popu)

    for i in datos:
        ruido2.append(i[7])
    # se guarda el valor minimo para comparar
    menos_ruidosa = min(ruido2)
    print("hay cancion mas popular y menos ruidosa?:")
    # recorremos el ciclo nuevamente y comparamos de acuerdo al condicional
    for i in datos:
        if menos_ruidosa == i[7] and maximo == i[13]:
            # si lo encuentra imprime el indice i de datos (nuestro lista generada)
            print("si, hay una cancion que es la mas popular y la menos ruidosa:", i)
            contador = contador + 1
    if contador == 0:
        # si no cumple imprime que no existe
        print("no, hay cancion con esas caracteríticas")


if __name__ == '__main__':
    # main
    listar_archivo(datos)
    artistas(datos)
    ruido(datos)
    mas_bailables(datos)
    maspopu_menosruidosa(datos)
